import numpy as np


def only_transformed_bool_to_description(only_transformed_results, prefix=""):
    if only_transformed_results:
        return "{} only transformed features".format(prefix)
    else:
        return "{} combined features".format(prefix)

def plot_reg_fit(ax1, results_bore, results_pure=None, model_name="xgboost", experiment_name="", order=1, ci=80,
                 xfeature="subsample_number", yfeature="score", only_transformed=False,
                 color_sequence=["green", "blue", "grey"], PATCHES = None,
                xlabel="number of samples", ylabel="f1-score"):
    x_labels = np.unique(results_bore[xfeature])
    x_range = np.arange(len(x_labels))
    mapping_dict = dict(zip(x_labels, x_range))
    ax1.set_xlim([-0.1, max(x_range)+0.1])
     
    if PATCHES is None:
        PATCHES = []
    labels = []
    for i, (title, results) in enumerate(results_bore.groupby("only_transformed")):
        if not only_transformed or title:
            sns.regplot(results[xfeature].apply(lambda x: mapping_dict[x]),results[yfeature], 
                        scatter=True, ax=ax1, label=title, ci=ci,
                        color=OCADO_COLORS[color_sequence[i]], order=order,
                       x_bins=x_range, x_ci=50)
            PATCHES.append(mpatches.Patch(color=OCADO_COLORS[color_sequence[i]],
                                          label=only_transformed_bool_to_description(title, model_name)))
    if results_pure is not None:
        sns.regplot(results_pure[xfeature].apply(lambda x: mapping_dict[x]), results_pure[yfeature], 
                scatter=True, ax=ax1, label=title, ci=ci,
                color=list(OCADO_COLORS.values())[2], order=order, x_bins=x_range, x_ci=50)
        PATCHES.append(mpatches.Patch(color=OCADO_COLORS[color_sequence[2]],
                                      label=model_name))

    ax1.legend(handles=PATCHES)
    ax1.set_xticks(x_range)
    ax1.set_xticklabels(x_labels)
    ax1.set_xlabel(xlabel)
    ax1.set_ylabel(ylabel)
    return PATCHES
    
def boxplots(ax1, results_bore, results_pure, model_name, xfeature, yfeature, xlabel, ylabel, skipper):    
    PATCHES = []
    labels = []
    for i, (title, results) in enumerate(results_bore.groupby("only_transformed")):
        if i>skipper:
            labels.append(title)
            mydf = pd.DataFrame()
            DFS = []
            for label, df in results.groupby(xfeature):
                DFS.append(pd.DataFrame({label: df[yfeature].values}))   
            mydf = pd.concat(DFS, axis=1)
            mydf.plot(kind="box", ax=ax1, color=list(OCADO_COLORS.values())[i], whis="range")
            PATCHES.append(mpatches.Patch(color=list(OCADO_COLORS.values())[i],
                                          label=only_transformed_bool_to_description(title, model_name)))
        else:
            print("")

    mydf = pd.DataFrame()
    DFS = []
    for label, df in results_pure.groupby(xfeature):
        DFS.append(pd.DataFrame({label: df[yfeature].values}))    
    mydf = pd.concat(DFS, axis=1)
    mydf.plot(kind="box", ax=ax1, color=list(OCADO_COLORS.values())[2], whis="range")
    PATCHES.append(mpatches.Patch(color=list(OCADO_COLORS.values())[2],
                                  label=model_name))


    ax1.legend(handles=PATCHES)
    ax1.set_xlabel(xlabel)
    ax1.set_ylabel(ylabel)